package org.mian.gitnex.helpers.codeeditor;

/**
 * @author AmrDeveloper
 * @author M M Arif
 */

public enum LanguageName {
	JAVA,
	PYTHON,
	GO_LANG
}
